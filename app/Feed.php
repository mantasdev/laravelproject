<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{

    protected $table = 'feed';

    protected $fillable = ['provider', 'article_url', 'image', 'title',
                           'author', 'description', 'article',
                           'created_at', 'updated_at'];


    public $timestamps = false;
}
