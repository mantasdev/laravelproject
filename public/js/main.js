jQuery( function( $ ) {
   var target = $( '.read-more' );

    target.on( 'click', function( event ) {

       event.preventDefault( );

       var desc = $( this ).parent().find( '.description' ).text( );
       var title = $( this ).parent().find( 'h2' ).text( );
       var articleUrl = $( this ).parent().find( 'a' ).attr('href');

        // Modal selectors
       var modalTitle = $('.modal-title');
       var modalBody = $('.modal-body');
       var modalHref = $('.modal-footer a');

       // Add details to modal
       modalTitle.html(title);
       modalBody.html(desc);
       modalHref.attr("href", articleUrl)


   });
});