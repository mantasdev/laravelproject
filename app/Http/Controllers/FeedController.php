<?php

namespace App\Http\Controllers;


use App\Feed;
use App\Repository\FeedRepository;
use Illuminate\Http\Request;
use App\Repositories\NewsFeedRepository;

class FeedController extends Controller
{

    protected $model;

    public function __construct(Feed $feed)
    {

        $this -> model = new FeedRepository($feed);

    }


    // Get all post from database
    public function index()
    {

        // Records with pagination default is 15
        $articles = $this -> model -> paginate();

        return view('pages/feed', ['articles' => $articles ]);

    }

    public function search(Request $request)
    {
        /*
         * @todo: validation for inputs
         */
        $articles = $this -> model -> where('provider', $request -> input('article_provider') );

        return view('pages/feed', ['articles' => $articles ]);

    }

}
