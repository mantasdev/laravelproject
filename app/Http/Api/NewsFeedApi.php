<?php

namespace App\Http\Api;

use App\Feed;
use App\Http\Api\Abstracts\BaseNewsFeedAbstract;



class NewsFeedApi extends BaseNewsFeedAbstract
{
    /**
     * NewsFeedApi constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

    }


    /*
     * Save news feed article to database
     */
    public function save(array $attributes)
    {

        $article = Feed::firstOrNew( ['article_url' => $attributes[ 'feedUrl' ] ] );

        $article -> provider = $attributes[ 'sourceName' ];
        $article -> article_url = $attributes[ 'feedUrl' ];

        $article -> image = isset($attributes[ 'feedImageSrc' ])
            ? $attributes[ 'feedImageSrc' ] : 'https://imgplaceholder.com/420x320/ff7f7f/333333/fa-image';

        $article -> title = $attributes[ 'feedTitle' ];
        $article -> author = $attributes[ 'feedTitle' ];

        $article -> description = isset($attributes[ 'feedDescription' ])
            ? $attributes[ 'feedDescription' ] : 'Description was not found';

        $article -> article = isset($attributes[ 'feedContent' ]) ? $attributes[ 'feedContent' ] : 'Sorry, no contents';

        $article -> save();

    }

}