<?php

namespace App\Repository;

interface RepositoryInterface
{

    public function all();

    public function create(array $data);

    public function paginate($perPage = 15, $columns = array('*'));

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);

}