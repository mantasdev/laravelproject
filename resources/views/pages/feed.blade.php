@extends('layouts.app')

@section('content')
    <div class="search col-md-12">

        <form method="post" action="{{route('searchFeed')}}">
            @csrf
            <div class="form-group">
                <label for="name">Article provider</label>
                <select name="article_provider" id="article_provider">
                    <option value="Diena.lt">Diena.lt</option>
                    <option value="Delfi.lt">Delfi.lt</option>
                    <option value="Snob.ru">Snob.ru</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Search</button>
        </form>

    </div>
    <section class="news-feed row col-md-12">

        @if( !$articles -> isEmpty() )

            @foreach($articles as $article)

                <article class="col-md-6 col-sm-12">
                    <a target="_blank" href="{{ $article -> article_url }}">
                    <img src="{{$article -> image}}" alt="image_holder">
                   <h2>{{ $article -> title }}</h2></a>
                    <span class="author">{{ $article -> author }} </span>
                    <p>{{ $article -> description }}</p>
                    <a class="read-more" href="#"  data-toggle="modal" data-target="#articleModal">Read More</a>
                    <p class="description">{{ $article -> description  }}</p>
                </article>

            @endforeach

    </section>

    <!-- Modal -->
    <div class="modal fade" id="articleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <a href="#" class="btn btn-primary">Go to article page</a>
                </div>
            </div>
        </div>
    </div>
    <div class="pagination">
        <?= $articles -> render(); ?>
    </div>

    @else

        <h4>Opps , nothing was found</h4>

    @endif

@stop