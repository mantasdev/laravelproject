<?php

namespace App\Console\Commands;

use App\Http\Api\NewsFeedApi;
use Illuminate\Console\Command;

class CreateNewsFeedPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CreateNewsFeedPosts:createnewsfeed';

    protected $api;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create news feed posts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this -> api = new NewsFeedApi();

        $countryPrefix = ['lt','lv','ru','pl'];
        $randKeys = array_rand($countryPrefix,1);

        $content =  $this -> api -> get( [ 'endpoint' => 'top-headlines', 'country' => $countryPrefix[ $randKeys ] ] );

        $this -> saveFeed($content);

    }

    public function saveFeed(array $feeds)
    {
        // Insert every feed post to database.
        // @todo : Content validation
        foreach ($feeds['articles'] as $feed):

            //Iinformation which we will insert to database.
            $article[ 'sourceName' ]      = $feed[ 'source' ][ 'name' ];
            $article[ 'authorName' ]      = $feed[ 'author' ];
            $article[ 'feedTitle' ]       = $feed[ 'title' ];
            $article[ 'feedUrl' ]         = $feed[ 'url' ];
            $article[ 'feedDescription' ] = $feed[ 'description' ];
            $article[ 'feedImageSrc' ]    = $feed[ 'urlToImage' ];
            $article[ 'feedContent' ]     = $feed[ 'content' ];


            $this -> api -> save($article);

        endforeach;
    }
}
