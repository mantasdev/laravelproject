<?php

namespace App\Providers;
use App\Repositories\NewsFeedRepository;
use App\Repositories\Interfaces\NewsFeedInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {
        $this->app->bind(
            NewsFeedRepository::class
        );
    }

}