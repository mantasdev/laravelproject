<?php

namespace App\Repository;

use  App\Repository\RepositoryInterface;

use Illuminate\Database\Eloquent\Model;


class FeedRepository implements RepositoryInterface
{

    protected $model;


    public function __construct(Model $model)
    {

        $this -> model = $model;
    }


    // return all articles
    public function all($columns = array('*'))
    {

        return $this->model->get($columns);

    }


    // create new article
    public function create(array $data)
    {

        return $this -> model -> create($data);

    }


    // update article with another data and given id
    public function update(array $data, $id)
    {

        $article = $this -> find($id);

        return $article -> update($data);

    }


    // Show article with given id
    public function show($id)
    {

        return $this -> model - findOrFail($id);

    }


    // delete content
    public function delete($id)
    {

        return $this -> model -> destroy($id);

    }


    public function paginate($perPage = 15, $columns = array('*')) {

        return $this->model->paginate($perPage, $columns);

    }


    public function where($attribute , $data )
    {
        return $this -> model -> where($attribute, '=', $data) -> paginate(15);
    }

}