<?php

namespace App\Http\Api\Abstracts;

use Illuminate\Database\Eloquent\Model;
use \GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;


abstract class BaseNewsFeedAbstract
{

    protected $apiKey;

    protected $serviceUrl;

    /**
     * BaseNewsFeedAbstract constructor.
     *
     */
    public function __construct()
    {

        $this -> apiKey = env('newsFeedApi');

        $this -> serviceUrl = 'https://newsapi.org/v2/';

    }

    /**
     * @param array $parameters
     *
     * @return mixed
     */
    public function get(array $parameters)
    {

        $client = new Client;

        $url = $this -> serviceUrl .
            $parameters[ 'endpoint' ] . '?country=' . $parameters[ 'country' ] . '&apiKey='
            . $this -> apiKey;

        $request = $client -> request('GET', $url, ['verify' => false]);

        $feed = $request -> getBody();

        return json_decode($feed, true);
    }
}